package = "restore"
version = "scm-1"
source = {
   url = "https://gitlab.com/ochaton/restore.git",
}
description = {
   homepage = "https://gitlab.com/ochaton/restore",
   license = "MIT"
}
dependencies = {
   "tarantool >= 2.8"
}
build = {
   type = "builtin",
   modules = {
      restore = "restore.lua"
   }
}
