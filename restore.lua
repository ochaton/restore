#!/usr/bin/env tarantool

local buffer = require "buffer"
local fiber = require "fiber"
local ffi = require 'ffi'
local fio = require 'fio'
local log = require 'log'
local fun = require 'fun'
local key_def = require 'key_def'
local msgpack = require 'msgpack'
local json = require 'json'
local xlog = require 'xlog'
local errno = require 'errno'

local function assert(cond, ...)
	if cond then
		return cond, ...
	end
	error(tostring(...))
end

local function panic(fmt, ...)
	local msg = fmt:format(...)
	error(msg, 0)
end

local Index = {}
Index.__index = Index

function Index:__serialize()
	local parts = self.parts
	if self._space and self._space.format then
		parts = {}
		for _, part in pairs(self.parts) do
			table.insert(parts, self._space.format[part[1]+1].name)
		end
	end
	return ("Index<%d:%s:%s>%s"):format(
		self.id, self.name, self.unique and "unique" or "non-unique",
		json.encode(parts)
	)
end

function Index.new(index_tuple, restore)
	return setmetatable({
		_restore = restore,
		kd = key_def.new(fun.iter(index_tuple[6])
			:map(function(t) return { fieldno = t[1]+1, type = t[2] } end)
			:totable()),
		id = index_tuple[2],
		name = index_tuple[3],
		parts = index_tuple[6],
		unique = index_tuple[5].unique,
	}, Index)
end

function Index:select(key, opts)
	if self.id == 0 then
		return self._space:select(key, opts)
	end

	opts = opts or {}
	opts.limit = math.min(opts.limit or 1000, 1000)
	return self:pairs(key, opts):totable()
end

function Index:count(key)
	return self:pairs(key):length()
end

function Index:pairs(key, opts)
	opts = opts or {}
	local kd = self.kd

	if self.id == 0 then
		return self._space:pairs(key, opts)
	end

	if type(key) ~= 'table' then
		key = {key}
	end

	local iter = self._space._restore:pairs({space_id = self._space.id})
		:grep(function(b) return kd:compare_with_key(b.tuple, key) == 0 end)
		:map(function(b) return b.tuple end)

	if opts.limit then
		iter = iter:take_n(opts.limit)
	end

	return iter
end

function Index:get(key, opts)
	return self:pairs(key, opts):nth(1)
end

function Index:dump(key, opts)
	opts = opts or {}
	if type(key) ~= 'table' then
		key = {key}
	end

	local kstr = table.concat(key, "_")
	if #kstr == 0 then
		kstr = "_"
	end

	local file
	if not opts.name then
		file = table.concat({
			fio.basename(self._space._restore.path):gsub("%.snap$",""),
			self._space.name,
			self.name,
			kstr,
			os.date('%F_%H%M%S'),
			'dump',
		}, ".")
	else
		file = table.concat({
			opts.name,
			self._space.name,
			self.name,
			kstr,
			os.date('%F_%H%M%S'),
			'dump'
		}, '.')
	end

	local fh = assert(fio.open(file, {'O_CREAT', 'O_EXCL', 'O_WRONLY'}, tonumber("0644", 8)))

	local total = self:pairs(key, opts)
		:map(function(tuple)
			assert(fh:write(msgpack.encode(tuple)))
			return 1
		end)
		:length()

	assert(fh:close())
	log.info("closing file: %s", file)
	return total
end

local Space = {}
Space.__index = Space

function Space:__serialize()
	return { id = self.id, name = self.name, format = self.format, index = self.index }
end

function Space.new(space_tuple, restore)
	return setmetatable({
		_restore = restore,
		id = space_tuple[1],
		name = space_tuple[3],
		format = setmetatable(space_tuple[7], {__serialize='sequence'}),

		tomap = function(t)
			local rv = {}
			for i, field in ipairs(space_tuple[7]) do
				rv[field.name] = t[i]
			end
			return rv
		end,
	}, Space)
end

function Space:select(key, opts)
	opts = opts or {}
	opts.limit = math.min(opts.limit or 1000, 1000)
	return self:pairs(key, opts):totable()
end

function Space:get(key, opts)
	return self:pairs(key, opts):nth(1)
end

function Space:count()
	return self:pairs():length()
end

function Space:pairs(key, opts)
	opts = opts or {}

	if type(key) ~= 'table' then
		key = {key}
	end

	local kd = self.index[0].kd
	local space_id = self.id

	local iter = self._restore:pairs({space_id = space_id})

	if #key > 0 then
		iter = iter
			:take_while(function(b) return kd:compare_with_key(b.tuple, key) <= 0 end)
			:grep(function(b) return kd:compare_with_key(b.tuple, key) == 0 end)
	end

	iter = iter:map(function(b) return b.tuple end)

	if opts.limit then
		iter = iter:take_n(opts.limit)
	end

	return iter
end

local restore = {}
restore.__index = restore

function restore:load_schema()
	local spaces = self:pairs()
		:drop_while(function(b) return b.space_id ~= 280 end)
		:take_while(function(b) return b.space_id == 280 end)
		:map(function(b) return b.tuple end)
		:totable()

	local indexes = self:pairs()
		:drop_while(function(b) return b.space_id ~= 288 end)
		:take_while(function(b) return b.space_id == 288 end)
		:map(function(b) return b.tuple end)
		:totable()

	collectgarbage('collect')

	local id2space = {}
	self.space = {}
	for _, def in ipairs(spaces) do
		if def[1] >= 0 then
			local space = Space.new(def, self)
			self.space[space.name] = space
			space.index = {}
			id2space[def[1]] = space
		end
	end

	for _, ind in ipairs(indexes) do
		local index = Index.new(ind, self)
		local space = id2space[ind[1]]
		index._space = space
		space.index[index.name] = index
		space.index[index.id] = index
	end

	self.id2space = id2space
	return self
end

function restore:pairs(opts)
	opts = opts or {}

	local prev_space
	local no = 0
	local space_len = 0
	local cur_space
	local iter = xlog.pairs(self.path)
		:map(function(t)
			no = no + 1
			if no % 1e5 == 0 then
				log.warn("%s - %.1fM lines", cur_space, tonumber(no)/1e6)
				fiber.yield()
			end

			t = t.BODY

			if prev_space ~= t.space_id then
				if prev_space then
					if self.id2space then
						log.warn("Space %s/%s finished => %d", prev_space, (self.id2space[prev_space] or {}).name, space_len)
					else
						log.warn("Space %s finished => %d", prev_space, space_len)
					end
				end
				space_len = 0
				if self.id2space then
					log.warn("Space %s/%s started", t.space_id, (self.id2space[t.space_id] or {}).name)
					cur_space = (self.id2space[t.space_id] or {}).name or t.space_id
				else
					log.warn("Space %s started", t.space_id)
					cur_space = 'space_'..t.space_id
				end
				prev_space = t.space_id
			end

			space_len = space_len + 1
			return t
		end)

	if opts.space_id then
		local space_seen = false
		local space_id = opts.space_id
		iter = iter
			:take_while(function(t)
				if space_seen then
					return t.space_id == space_id
				end
				if t.space_id == space_id then
					space_seen = true
				end
				return true
			end)
			:grep(function(t) return t.space_id == space_id end)
	end

	return iter
end

function restore.new(_, path)
	if not fio.path.exists(path) then
		panic('path %s does not exists', path)
	end
	if fio.path.is_file(path) then
		if not path:match('%.snap$') then
			panic('file "%s" is not snapshot', path)
		end
	elseif fio.path.is_dir(path) then
		local list = assert(fio.listdir(path))
		list = fun.grep('%.snap$', list):totable()
		table.sort(list)
		log.info("ls %s: %s", fio.pathjoin(path, '*.snap'), table.concat(list, ','))
		if #list == 0 then
			panic('no snapshot files found in %s', path)
		end
		path = fio.pathjoin(path, list[#list])
		if not fio.path.is_file(path) then
			panic('path: %s is not a file', path)
		end
	end

	local self = setmetatable({ path = path }, restore)
	self:load_schema()

	return self
end
setmetatable(restore, {__call=restore.new})


local BUFSIZ = 1024*1024

-- Decode the next tuple stored in the stream buffer and
-- advance the buffer position accordingly. Return nil if
-- the decoder failed.
local function space_stream_next_tuple(stream)
    local status, tuple, rpos = pcall(msgpack.decode,
        stream.buf.rpos, stream.buf:size())
    if not status then
        return nil
    end
    stream.buf.rpos = rpos
    return box.tuple.new(tuple)
end

-- Read in more data from the dump file to the stream buffer.
-- Return the number of bytes read on success, nil on failure.
local function space_stream_read(stream)
    if stream.buf.rpos ~= stream.buf.buf then
        -- Move whatever is left in the buffer
        -- to the beginning.
        local buf = buffer.ibuf(BUFSIZ)
        ffi.copy(buf:alloc(stream.buf:size()), stream.buf.rpos, stream.buf:size())
        stream.buf:recycle()
        stream.buf = buf
    end
    local len = stream.fh:read(stream.buf:reserve(BUFSIZ), BUFSIZ)
    if not len or len == 0 then
        stream.fh:close()
    end
    if not len then
        return nil, string.format("Failed to read file %s, errno %d, (%s)",
            stream.path, errno(), errno.strerror())
    end
    stream.buf:alloc(len)
    return len
end


local loader = {}
loader.__index = loader


function loader.new(_, path)
	if not fio.path.exists(path) then
		panic('path %s does not exists', path)
	end
	if not fio.path.is_file(path) then
		panic('path "%s" is not a file', path)
	end
	if not path:match("%.dump$") then
		panic("path %s must end with .dump", path)
	end

	return setmetatable({ path = path }, loader)
end

setmetatable(loader, {__call = loader.new})

function loader:pairs()
	self.fh = assert(fio.open(self.path, {'O_RDONLY'}))

	local buf = buffer.ibuf(1024*1024)

	return fun.iter(function(ctx, state)
		local tuple = space_stream_next_tuple(ctx)
		if tuple then
			return tuple, tuple
		end

		local len, err = assert(space_stream_read(ctx))
		if len == 0 and ctx.buf:size() > 0 then
			panic("Failed to decode tuple: trailing bytes in the input stream %s", ctx.path)
		end
		if len == 0 then
			return nil
		end
		return 1, false
	end, { buf = buf, fh = self.fh, path = self.path }):grep(fun.op.truth)
end


_G.loader = loader
_G.restore = restore
