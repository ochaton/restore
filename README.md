# Tarantool Snapshot restore

Requires tarantool ≥ 2.8

## How to install

Copy file `restore.lua` into your code.

## How to run

```bash
tarantool -i /path/to/restore.lua

Tarantool 2.8.4-0-g47e6bd362
type 'help' for interactive help
tarantool> r = restore('./00000000000000197661.snap')
---
...
tarantool> r.space._space:select()
---
- - [257, 1, '_vinyl_deferred_delete', 'blackhole', 0, {'group_id': 1}, [{'name': 'space_id',
        'type': 'unsigned'}, {'name': 'lsn', 'type': 'unsigned'}, {'name': 'tuple',
        'type': 'array'}]]
  - [272, 1, '_schema', 'memtx', 0, {}, [{'type': 'string', 'name': 'key'}]]
  - [276, 1, '_collation', 'memtx', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {
        'name': 'name', 'type': 'string'}, {'name': 'owner', 'type': 'unsigned'},
      {'name': 'type', 'type': 'string'}, {'name': 'locale', 'type': 'string'}, {
        'name': 'opts', 'type': 'map'}]]
  - [280, 1, '_space', 'memtx', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {'name': 'owner',
        'type': 'unsigned'}, {'name': 'name', 'type': 'string'}, {'name': 'engine',
        'type': 'string'}, {'name': 'field_count', 'type': 'unsigned'}, {'name': 'flags',
        'type': 'map'}, {'name': 'format', 'type': 'array'}]]
  - [281, 1, '_vspace', 'sysview', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {'name': 'owner',
        'type': 'unsigned'}, {'name': 'name', 'type': 'string'}, {'name': 'engine',
        'type': 'string'}, {'name': 'field_count', 'type': 'unsigned'}, {'name': 'flags',
        'type': 'map'}, {'name': 'format', 'type': 'array'}]]
  - [284, 1, '_sequence', 'memtx', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {'name': 'owner',
        'type': 'unsigned'}, {'name': 'name', 'type': 'string'}, {'name': 'step',
        'type': 'integer'}, {'name': 'min', 'type': 'integer'}, {'name': 'max', 'type': 'integer'},
      {'name': 'start', 'type': 'integer'}, {'name': 'cache', 'type': 'integer'},
      {'name': 'cycle', 'type': 'boolean'}]]
  - [285, 1, '_sequence_data', 'memtx', 0, {}, [{'name': 'id', 'type': 'unsigned'},
      {'name': 'value', 'type': 'integer'}]]
  - [286, 1, '_vsequence', 'sysview', 0, {}, [{'name': 'id', 'type': 'unsigned'},
      {'name': 'owner', 'type': 'unsigned'}, {'name': 'name', 'type': 'string'}, {
        'name': 'step', 'type': 'integer'}, {'name': 'min', 'type': 'integer'}, {
        'name': 'max', 'type': 'integer'}, {'name': 'start', 'type': 'integer'}, {
        'name': 'cache', 'type': 'integer'}, {'name': 'cycle', 'type': 'boolean'}]]
  - [288, 1, '_index', 'memtx', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {'name': 'iid',
        'type': 'unsigned'}, {'name': 'name', 'type': 'string'}, {'name': 'type',
        'type': 'string'}, {'name': 'opts', 'type': 'map'}, {'name': 'parts', 'type': 'array'}]]
  - [289, 1, '_vindex', 'sysview', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {'name': 'iid',
        'type': 'unsigned'}, {'name': 'name', 'type': 'string'}, {'name': 'type',
        'type': 'string'}, {'name': 'opts', 'type': 'map'}, {'name': 'parts', 'type': 'array'}]]
  - [296, 1, '_func', 'memtx', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {'name': 'owner',
        'type': 'unsigned'}, {'name': 'name', 'type': 'string'}, {'name': 'setuid',
        'type': 'unsigned'}]]
  - [297, 1, '_vfunc', 'sysview', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {'name': 'owner',
        'type': 'unsigned'}, {'name': 'name', 'type': 'string'}, {'name': 'setuid',
        'type': 'unsigned'}]]
  - [304, 1, '_user', 'memtx', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {'name': 'owner',
        'type': 'unsigned'}, {'name': 'name', 'type': 'string'}, {'name': 'type',
        'type': 'string'}, {'name': 'auth', 'type': 'map'}]]
  - [305, 1, '_vuser', 'sysview', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {'name': 'owner',
        'type': 'unsigned'}, {'name': 'name', 'type': 'string'}, {'name': 'type',
        'type': 'string'}, {'name': 'auth', 'type': 'map'}]]
  - [312, 1, '_priv', 'memtx', 0, {}, [{'name': 'grantor', 'type': 'unsigned'}, {
        'name': 'grantee', 'type': 'unsigned'}, {'name': 'object_type', 'type': 'string'},
      {'name': 'object_id', 'type': 'scalar'}, {'name': 'privilege', 'type': 'unsigned'}]]
  - [313, 1, '_vpriv', 'sysview', 0, {}, [{'name': 'grantor', 'type': 'unsigned'},
      {'name': 'grantee', 'type': 'unsigned'}, {'name': 'object_type', 'type': 'string'},
      {'name': 'object_id', 'type': 'scalar'}, {'name': 'privilege', 'type': 'unsigned'}]]
  - [320, 1, '_cluster', 'memtx', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {'name': 'uuid',
        'type': 'string'}]]
  - [330, 1, '_truncate', 'memtx', 0, {}, [{'name': 'id', 'type': 'unsigned'}, {'name': 'count',
        'type': 'unsigned'}]]
  - [340, 1, '_space_sequence', 'memtx', 0, {}, [{'name': 'id', 'type': 'unsigned'},
      {'name': 'sequence_id', 'type': 'unsigned'}, {'name': 'is_generated', 'type': 'boolean'}]]
```

### How to dump my space

```lua
tarantool> r.space.my_space.index[0]:dump({ key }, { name = 'dump_all' })
closing file: dump_all.my_space.primary._.2022-11-11_154116.dump
```

### How to load dump

```lua
tarantool> l = loader('dump_all.my_space.primary._.2022-11-11_154116.dump')
tarantool> l:pairs():length()
---
- 39213
...
```
